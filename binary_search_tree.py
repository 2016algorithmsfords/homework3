# -*- coding: utf-8 -*-
"""
@author: gkalmanovich
"""

class node:
	value = 0
	left  = None
	right = None

def insert(p,x):
    if (p==None):
    	p = node()
    	p.value = x
    else:
        if x < p.value:
        	p.left  = insert(p.left ,x)
        else:
        	p.right = insert(p.right,x)
    return p
    
def print_tree(p,depth=0):
	# prints tree sideways
	if (p.right!=None):
		print_tree(p.right, depth+1)
	print("    "*depth+str(p.value))
	if (p.left !=None):
		print_tree(p.left , depth+1)
    
def lookup(p,x):
    # have this function return "true" if x is a member of the subtree p and "false" otherwise
    placeholder = x
    
def delete(p,x):
	# if x is not a member of the substructure, nothing happens and p is returned
	# if x is a member of the substructure, the function removes an instance of it and 
	# returns the head of the substructure (usually p, unless p got removed, then it is the new head)
    placeholder = x
    
