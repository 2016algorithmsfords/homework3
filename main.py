# -*- coding: utf-8 -*-
"""
@author: gkalmanovich
"""

import binary_search_tree
    

head = None
head = binary_search_tree.insert(head, 1)
head = binary_search_tree.insert(head, 2)
head = binary_search_tree.insert(head, 3)
head = binary_search_tree.insert(head, 1)

print(head.value)
print(head.right.value)
print(head.right.right.value)
print(head.right.left.value)

print
print

binary_search_tree.print_tree(head)